/* eslint-disable no-useless-catch */
const CONFIG = require('../Config')
const ERROR = CONFIG.responseMessages.ERROR

// count of nominals in ATM
const limits = { 2000: 100, 500: 1000, 200: 1000, 100: 1000, 50: 100 }

const recur = async (amount, nominals) => {
  try {
    if (amount === 0) return {} // success
    if (!nominals.length) return 0 // failure
    const nominal = nominals[0]
    const count = Math.min(limits[nominal], Math.floor(amount / nominal))
    for (let i = count; i >= 0; i--) {
      const result = await recur(amount - i * nominal, nominals.slice(1))
      if (result) return i ? { [nominal]: i, ...result } : result
    }
  } catch (error) {
    throw error
  }
}

exports.getWithdrawlMoney = async (queryData, h) => {
  try {
    let arr = Object.keys(limits).map(Number).sort((a, b) => b - a)
    if (queryData.preferDenomination) arr = arr.splice(arr.indexOf(queryData.preferDenomination), arr.length)

    const result = await recur(queryData.money, arr)

    if (typeof result === 'undefined') {
      throw ERROR.NO_SUFFICIENT_BALANCE
    } else {
      Object.keys(result).map((item) => {
        if (typeof limits[item] !== 'undefined' && typeof result[item] !== 'string') limits[item] -= result[item]
      })
    }

    return result
  } catch (err) {
    throw err
  }
}
