/**
 * Created by rajat on 13/05/19.
 */

'use strict'

const Inert = require('inert')
const Vision = require('vision')
const HapiSwagger = require('hapi-swagger')

const Logger = require('../Libs/logger').logger

const swaggerOptions = {
  info: {
    title: 'API Documentation',
    version: '1.0'
  },
  grouping: 'tags',
  cors: true
}

exports.plugin = {
  name: 'swagger-plugin',
  register: async (server) => {
    await server.register([
      Inert,
      Vision,
      {
        plugin: HapiSwagger,
        options: swaggerOptions
      }
    ])
    Logger.trace('Swagger Loaded')
  }
}
