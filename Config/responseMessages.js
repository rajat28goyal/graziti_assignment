/**
 * Created by rajat on 13/05/19.
 */

'use strict'

exports.ERROR = {
  UNAUTHORIZED: {
    statusCode: 401,
    message: {
      en: 'You are not authorized to perform this action',
      ar: 'لا تملك الصلاحية لتنفيذ هذا الإجراء'
    },
    type: 'UNAUTHORIZED'
  },
  INVALID_OBJECT_ID: {
    statusCode: 400,
    message: {
      en: 'Invalid Id provided.',
      ar: 'قدمت رقم غير صالح.'
    },
    type: 'INVALID_OBJECT_ID'
  },
  DB_ERROR: {
    statusCode: 400,
    message: {
      en: 'DB Error : ',
      ar: 'DB خطأ:'
    },
    type: 'DB_ERROR'
  },
  APP_ERROR: {
    statusCode: 400,
    message: {
      en: 'Application Error ',
      ar: 'خطأ في تطبيق'
    },
    type: 'APP_ERROR'
  },
  DUPLICATE: {
    statusCode: 400,
    message: {
      en: 'Duplicate Entry',
      ar: 'إدخال مكرر'
    },
    type: 'DUPLICATE'
  },
  DEFAULT: {
    statusCode: 400,
    message: {
      en: 'Something went wrong.',
      ar: 'هناك خطأ ما.'
    },
    type: 'DEFAULT'
  },
  NO_SUFFICIENT_BALANCE: {
    statusCode: 400,
    message: {
      en: 'Denomination Not Found With entered Amount',
      ar: 'هناك خطأ ما.'
    },
    type: 'NO_SUFFICIENT_BALANCE'
  }
}

exports.SUCCESS = {
  DEFAULT: {
    statusCode: 200,
    message: {
      en: 'Success',
      ar: 'نجاح'
    },
    type: 'DEFAULT'
  }
}
