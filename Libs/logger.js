/**
 * Created by rajat on 13/05/19.
 */

'use strict'

const log4js = require('log4js')

log4js.configure({
  appenders: { out: { type: 'stdout', layout: { type: 'coloured' } } },
  categories: { default: { appenders: ['out'], level: 'ALL' } }
})

module.exports = {
  logger: log4js.getLogger('[logs]')
}
