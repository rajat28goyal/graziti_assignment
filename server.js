/**
 * Created by Kritika on 13/05/19.
 */

const Hapi = require('@hapi/hapi')
const Plugins = require('./Plugins')
const Routes = require('./Routes')
const Logger = require('./Libs/logger').logger

const serverOptions = {
  port: 8000,
  routes: {
    cors: true
  }
}

// Create Server
const server = new Hapi.Server(serverOptions)

global.server = server;

(async (initServer) => {
  // Register All Plugins
  await server.register(Plugins)

  server.views({
    engines: {
      html: require('handlebars')
    },
    relativeTo: __dirname,
    path: './templates'
  })

  // API Routes
  await server.route(Routes)

  server.events.on('response', (request) => {
    Logger.trace(`[${request.method.toUpperCase()} ${request.url.path}]
     : ${request.response.statusCode}`)
    Logger.trace({ INFO: request.payload })
  })

  // Default Routes
  server.route([
    {
      method: 'GET',
      path: '/',
      config: {
        auth: false
      },
      handler: (request, h) => {
        return h.view('index')
      }
    }
  ])

  // Start Server
  try {
    await server.start()
  } catch (error) {
    Logger.error(error)
    process.exit(1)
  }
  Logger.info(`Server running at ${server.info.uri}`)
  process.on('unhandledRejection', (error) => {
    Logger.error(error)
  })
  process.on('uncaughtException', (error) => {
    Logger.error(error)
  })
})()
