/**
 * Created by Kritika on 13/05/19.
 */

'use strict'

const Boom = require('boom')
const CONFIG = require('../Config')
const ERROR = CONFIG.responseMessages.ERROR
const SUCCESS = CONFIG.responseMessages.SUCCESS
const logger = require('../Libs/logger').logger

function sendError (language, data, h) {
  let error
  let finalMessage = ''

  logger.error({ ERROR: data })

  if (typeof data === 'object' && Object.prototype.hasOwnProperty.call(data, 'statusCode') && Object.prototype.hasOwnProperty.call(data, 'message')) {
    finalMessage = data.message[language]
  } else {
    // eslint-disable-next-line no-unused-vars
    if (typeof data === 'object') {
      if (data.name === 'MongoError') {
        finalMessage += ERROR.DB_ERROR.message[language]

        if (data.code === 11000) {
          finalMessage += ERROR.DUPLICATE.message[language]
        }
      } else if (data.name === 'ApplicationError') {
        finalMessage += ERROR.APP_ERROR.message[language]
      } else if (data.name === 'ValidationError') {
        finalMessage += ERROR.APP_ERROR.message[language] + data.message
      } else if (data.name === 'CastError') {
        finalMessage += ERROR.DB_ERROR.message[language] + ERROR.INVALID_OBJECT_ID.message[language]
      } else if (data.response) {
        finalMessage = data.response.message
      }
    } else {
      finalMessage = data
    }

    if (typeof finalMessage === 'string') {
      if (finalMessage.indexOf('[') > -1) {
        finalMessage = finalMessage.substr(finalMessage.indexOf('['))
      }

      finalMessage = finalMessage.replace(/"/g, '')
      finalMessage = finalMessage.replace('[', '')
      finalMessage = finalMessage.replace(']', '')
    }

    data.statusCode = 100
  }

  switch (data.statusCode) {
    case 100: error = Boom.boomify(finalMessage, {
      statusCode: data.statusCode
    })
      break
    case 400: error = Boom.badRequest(finalMessage)
      break
    case 401: error = Boom.unauthorized(finalMessage)
      break
    case 402: error = Boom.paymentRequired(finalMessage)
      break
    case 403: error = Boom.forbidden(finalMessage)
      break
    case 404: error = Boom.notFound(finalMessage)
      break
    case 405: error = Boom.methodNotAllowed('Method Is Not Allowed.')
      break
    case 406: error = Boom.notAcceptable('Not Acceptable.')
      break
    case 407: error = Boom.proxyAuthRequired('Proxy Authentication Required.')
      break
    case 408: error = Boom.clientTimeout('Request Time-out.')
      break
    case 409: error = Boom.conflict('There was a conflict.')
      break
    case 410: error = Boom.resourceGone('subscription has unsubscribed or expired.')
      break
    case 411: error = Boom.lengthRequired('Length Required.')
      break
    case 412: error = Boom.preconditionFailed()
      break
    case 413: error = Boom.entityTooLarge('Request Entity Too Large')
      break
    case 414: error = Boom.entityTooLarge('Request-URI Too Large')
      break
    case 415: error = Boom.unsupportedMediaType('Unsupported Media Type')
      break
    case 416: error = Boom.rangeNotSatisfiable()
      break
    case 417: error = Boom.expectationFailed()
      break
    case 418: error = Boom.teapot()
      break
    case 422: error = Boom.badData()
      break
    case 423: error = Boom.locked()
      break
    case 424: error = Boom.failedDependency()
      break
    case 428: error = Boom.preconditionRequired()
      break
    case 429: error = Boom.tooManyRequests()
      break
    case 451: error = Boom.illegal()
      break
    case 501: error = Boom.notImplemented()
      break
    case 502: error = Boom.badGateway()
      break
    case 503: error = Boom.serverUnavailable()
      break
    case 504: error = Boom.gatewayTimeout()
      break
    default: error = Boom.badImplementation()
      break
  }

  return error
}

function sendSuccess (language, successMsg, data, h) {
  successMsg = successMsg || SUCCESS.DEFAULT.message[language]

  if (typeof successMsg === 'object' && Object.prototype.hasOwnProperty.call(successMsg, 'statusCode') && Object.prototype.hasOwnProperty.call(successMsg, 'message')) {
    const finalMessage = successMsg.message[language]

    return h.response({ statusCode: successMsg.statusCode, message: finalMessage, data: data || {} }).code(200)
  } else return h.response({ statusCode: 200, message: successMsg, data: data || {} }).code(200)
}

function failActionFunction (request, h, error) {
  let customErrorMessage = ''

  error.output.payload.type = 'Joi Error'

  if (error.isBoom) {
    delete error.output.payload.validation

    if (error.output.payload.message.indexOf('authorization') !== -1) {
      error.output.statusCode = ERROR.UNAUTHORIZED.statusCode
      return error
    }
    const details = error.details[0]

    if (details.message.indexOf('pattern') > -1 && details.message.indexOf('required') > -1 && details.message.indexOf('fails') > -1) {
      error.output.payload.message = 'Invalid ' + details.path
      return error
    }
  }

  if (error.output.payload.message.indexOf('[') > -1) customErrorMessage = error.output.payload.message.substr(error.output.payload.message.indexOf('['))
  else customErrorMessage = error.output.payload.message

  customErrorMessage = customErrorMessage.replace(/"/g, '')
  customErrorMessage = customErrorMessage.replace('[', '')
  customErrorMessage = customErrorMessage.replace(']', '')

  error.output.payload.message = customErrorMessage.replace(/\b./g, (a) => a.toUpperCase())

  delete error.output.payload.validation
  return error
}

module.exports = {
  sendError: sendError,
  sendSuccess: sendSuccess,
  failActionFunction: failActionFunction
}
