/**
 * Created by rajat on 13/05/19.
 */

'use strict'

const UniversalFunc = require('../utils/universalFunctions')
const Joi = require('joi')
const Controller = require('../Controller').userController
const routes = []
const CONFIG = require('../Config')
const SUCCESS = CONFIG.responseMessages.SUCCESS

exports.getWithdrawlMoney = {
  method: 'GET',
  path: '/getWithdrawlMoney',
  config: {
    description: 'GET Money from ATM',
    tags: ['api', 'other'],
    auth: false,
    handler: (request, h) => {
      return Controller.getWithdrawlMoney(request.query, h)
        .then(response => {
          return UniversalFunc.sendSuccess('en', SUCCESS.DEFAULT, response, h)
        })
        .catch(error => {
          return UniversalFunc.sendError('en', error, h)
        })
    },
    validate: {
      query: {
        money: Joi.number().required(),
        preferDenomination: Joi.number().optional()
      },
      failAction: UniversalFunc.failActionFunction
    }
  }
}

for (const key in exports) {
  routes.push(exports[key])
}
module.exports = routes
